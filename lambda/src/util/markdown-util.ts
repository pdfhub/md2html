import * as commonmark from 'commonmark';


export namespace MarkdownUtil {

  export function convertToHTML(markdownStr: string, showDebugLog = false): (string | null) {
    if (showDebugLog) {
      console.log('MarkdownUtil: convertToHTML() with markdownStr = ' + markdownStr);
    }

    let htmlStr: (string | null) = null;
    try {
      let reader = new commonmark.Parser();
      let writer = new commonmark.HtmlRenderer({ safe: true });
      // writer.softbreak = "<br />";  // This throws excetion "this[type] is not a function"
      let parsed = reader.parse(markdownStr);
      if (showDebugLog) {
        console.log('MarkdownUtil: convertToHTML() successfully parsed: ' + parsed);
      }
      htmlStr = writer.render(parsed);
    } catch (ex) {
      console.log('MarkdownUtil: convertToHTML() Failed: error = ' + ex);
    }

    if (showDebugLog) {
      console.log('MarkdownUtil: convertToHTML() generated htmlStr = ' + htmlStr);
    }
    return htmlStr;
  }

}
